﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using sportsstore.Models.ViewModels;

namespace sportsstore.Infrastructure
{
    [HtmlTargetElement("div", Attributes = "page-model")]
    public class PageLinkTagHelper : TagHelper
    {
        private IUrlHelperFactory urlHelperFactory;

        public PageLinkTagHelper(IUrlHelperFactory helperFactory)
        {
            urlHelperFactory = helperFactory;
        }

        [ViewContext]
        [HtmlAttributeNotBound]
        public ViewContext ViewContext { get; set; }
        public PagingInfo PageModel { get; set; }
        public string PageAction { get; set; }

        public bool PageClassesEnabled { get; set; } = false;
        public string PageClass { get; set; }
        public string PageClassNormal { get; set; }
        public string PageClassSelected { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            IUrlHelper urlHelper = urlHelperFactory.GetUrlHelper(ViewContext);
            var divTagBuilder = new TagBuilder("div");
            for (int i = 1; i <= PageModel.TotalPages; i++)
            {
                var aTagBuilder = new TagBuilder("a");
                aTagBuilder.Attributes["href"] = urlHelper.Action(
                    PageAction, new { productPage = i });
                aTagBuilder.InnerHtml.Append(i.ToString());
                divTagBuilder.InnerHtml.AppendHtml(aTagBuilder);

                if (PageClassesEnabled)
                {
                    aTagBuilder.AddCssClass(PageClass);
                    aTagBuilder.AddCssClass(i == PageModel.CurrentPage ? 
                        PageClassSelected : 
                        PageClassNormal);
                }
            }
            output.Content.AppendHtml(divTagBuilder.InnerHtml);
        }
    }
}