﻿using Microsoft.AspNetCore.Mvc;
using sportsstore.Models;
using System.Linq;
using sportsstore.Models.ViewModels;

namespace sportsstore.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductRepository repository;
        public int PageSize = 2;

        public ProductController(IProductRepository repo)
        {
            this.repository = repo;
        }

        public ViewResult List(string category, int productPage = 1)
        {
            var productListViewModel = new ProductsListViewModel
            {
                Products = repository.Products
                    .Where(c => c.Category == null || c.Category == category)
                    .OrderBy(p => p.ProductID)
                    .Skip((productPage - 1) * PageSize)
                    .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = productPage,
                    ItemsPerPage = PageSize,
                    TotalItems = repository.Products.Count()
                },
                CurrentCategory = category
            };

            return View(productListViewModel);
        }
    }
}
