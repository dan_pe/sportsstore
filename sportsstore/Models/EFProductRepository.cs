﻿using System.Linq;
using Microsoft.AspNetCore.Identity.UI.Pages.Internal.Account;

namespace sportsstore.Models
{
    public class EFProductRepository : IProductRepository
    {
        private ApplicationDbContext context;
        private IQueryable<Product> _products;

        public EFProductRepository(ApplicationDbContext ctx)
        {
            this.context = ctx;
        }

        public IQueryable<Product> Products => this.context.Products;
    }
}