﻿using System.Linq;

namespace sportsstore.Models
{
    public interface IProductRepository
    {
        IQueryable<Product> Products { get; }
    }
}