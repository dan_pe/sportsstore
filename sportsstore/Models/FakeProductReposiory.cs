﻿using System.Collections.Generic;
using System.Linq;

namespace sportsstore.Models
{
    public class FakeProductReposiory : IProductRepository
    {
        public IQueryable<Product> Products => new List<Product>
        {
            new Product {Name = "Ball", Price = 25},
            new Product {Name = "Surfing desk", Price = 179},
            new Product {Name = "Running shoes", Price = 95}
        }.AsQueryable();
    }
}