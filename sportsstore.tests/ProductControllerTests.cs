﻿using System.Collections.Generic;
using System.Linq;
using Moq;
using sportsstore.Controllers;
using sportsstore.Models;
using sportsstore.Models.ViewModels;
using NUnit.Framework;

namespace sportsstore.tests
{
    [TestFixture]
    public class ProductControllerTests
    {
        private List<Product> productsList;

        private Mock<IProductRepository> productRepository;

        private ProductController subject;

        [SetUp]
        public void SetUp()
        {
            productsList = new List<Product>
            {
                new Product {ProductID = 1, Name = "P1"},
                new Product {ProductID = 2, Name = "P2"},
                new Product {ProductID = 3, Name = "P3"},
                new Product {ProductID = 4, Name = "P4"}
            };

            productRepository = new Mock<IProductRepository>();

            subject = new ProductController(productRepository.Object);
        }

        [Test]
        public void Can_Paginate()
        {
            productRepository
                .Setup(m => m.Products)
                .Returns(productsList.AsQueryable());

            subject.PageSize = 2;

            var result =
                subject.List(null,2).ViewData.Model as ProductsListViewModel;

            var prodArray = result.Products.ToArray();

            Assert.True(prodArray.Length == 2);
            Assert.AreEqual("P3", prodArray[0].Name);
            Assert.AreEqual("P4", prodArray[1].Name);
        }

        [Test]
        public void Can_Send_Pagination_View_Model()
        {
            productRepository
                .Setup(m => m.Products)
                .Returns((productsList).AsQueryable());

            subject.PageSize = 3;

            var result = subject.List(null, 2).ViewData.Model as ProductsListViewModel;

            var pageInfo = result.PagingInfo;

            Assert.AreEqual(2, pageInfo.CurrentPage);
            Assert.AreEqual(3, pageInfo.ItemsPerPage);
            Assert.AreEqual(4, pageInfo.TotalItems);
            Assert.AreEqual(2, pageInfo.TotalPages);
        }
    }
}